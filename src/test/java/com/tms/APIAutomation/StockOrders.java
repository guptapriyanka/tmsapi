package com.tms.APIAutomation;

import java.io.IOException;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.*;

public class StockOrders extends PropertiesReader {

	@Test(priority = 3)
	public void createStockOrder() throws IOException {
		String authorization = getObject("Authorization");
		RestAssured.baseURI = getObject("baseURI");

		String codeArticle = JsonParamaters.getStockArticle();
		System.out.println(codeArticle);

		String res = given().log().all().headers("Content-Type", "application/json")
				.header("Authorization", authorization)
				.body("{\n" + "\"code\":\"" + codeArticle + "\",\n" + "\"location\":\"DL1ANVR\",\n"
						+ "\"quantity\":100000\n" + "}")
				.when().post("/v1/stock/create").then().log().all().assertThat().statusCode(201).extract().response()
				.asString();

		System.out.println(res);

	}
	
	@Test(priority = 4)
	public void stockUpdate() throws IOException {
		try {
		String authorization = getObject("Authorization");
		RestAssured.baseURI = getObject("baseURI");

		String res = given().header("Content-Type", "application/json").header("Authorization", authorization)
				.body(payload.stockOrderUpdate()).when().post("/v1/stock/update").then().log().all().assertThat()
				.statusCode(200).extract().response().asString();
		
		System.out.println(res);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Test(priority = 5)
	public void stockList() throws IOException {

		try {
			String authorization = getObject("Authorization");
			RestAssured.baseURI = getObject("baseURI");

			String res = given().log().all().headers("Content-Type", "application/json")
					.header("Authorization", authorization).body(payload.stockOrderList()).when().get("/v1/stock/list")
					.then().log().all().assertThat().statusCode(200).extract().response().asString();

			System.out.println(res);
			JsonPath js = new JsonPath(res);
//		String codeArticleStockUpdate = js.getString(path)

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
