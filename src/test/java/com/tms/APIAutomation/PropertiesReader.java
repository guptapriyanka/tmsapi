package com.tms.APIAutomation;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class PropertiesReader {
	
	static Properties properties;
	
	public static void loadData() throws IOException {
		
		properties = new Properties();
		File f = new File(System.getProperty("user.dir")+"//config.properties");
		FileReader fileRead = new FileReader(f);
		properties.load(fileRead);
		
		
	}
	
	public static String getObject(String Data) throws IOException
	{
		loadData();
		String data = properties.getProperty(Data);
		return data;
		
	}

}
