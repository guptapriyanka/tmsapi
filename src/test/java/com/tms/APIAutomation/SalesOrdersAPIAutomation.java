package com.tms.APIAutomation;

import java.io.IOException;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;

public class SalesOrdersAPIAutomation extends PropertiesReader {

	@Test(priority = 1)
//	(groups = {"salesOrderAPICreationTest"})
	public void salesOrderAPICreation() throws IOException {
//		FileInputStream file = new FileInputStream(new File(".//JSONFileInput//JsonInputs.json"));
		try {
		String authorization = getObject("Authorization");
		RestAssured.baseURI = getObject("baseURI");
//Log all requests and responses	
//log().all()		
		for (int i = 0; i <= 10; i++) {
			String res = given().log().all().header("Content-Type", "application/json").header("Authorization", authorization)
					.body(payload.salesCreatePayload()).when().post("/v1/so/create").then().assertThat().statusCode(201).log().all()
					.extract().response().asString();

			System.out.println(res);
//			JsonPath js = new JsonPath(respn); // for parsing json
//			String testString = js.getString(respn);

		}
	}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	@Test(priority = 2)
	public void salesOrderAPIList() throws IOException {
		String authorization = getObject("Authorization");
		RestAssured.baseURI = getObject("baseURI");

		String res = given().header("Content-Type", "application/json").headers("Authorization", authorization)
				.body(payload.salesOrderList()).when().get("/v1/so/list").then().log().all().assertThat().statusCode(200).extract()
				.response().asString();

		System.out.println(res);
//		String respn = res.asString();
//		System.out.println(respn);

		JsonPath js = new JsonPath(res);
		String codeArticle = js.getString("details.salesOrders[6].articles[0].code");
		System.out.println(codeArticle);
		JsonParamaters.setStockArticle(codeArticle);
	}
}
