package com.tms.APIAutomation;

import java.util.Random;

public class RandomValues {

	private static String letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static char[] sRandom_Aplhanumeric = (letters + letters.toUpperCase() + "0123456789")
			.toCharArray();

	public String getRandomString(int length) {
		
		StringBuilder results = new StringBuilder();
		for(int i=0;i<length; i++)
		{
			results.append(sRandom_Aplhanumeric[new Random().nextInt(sRandom_Aplhanumeric.length)]);
		}
		
		return results.toString();
	}
}
