package com.tms.APIAutomation;

public class payload {
	
	public static String salesCreatePayload() {
	
		String sRndomValue = getRandomValue();
		return "{\n" + 
				"  \"orders\": [\n" + 
				"    {\n" + 
				"      \"code\": \"CODE"+sRndomValue+"\",\n" + 
				"      \"origin\": \"DL1ANVR\",\n" + 
				"      \"destination\": \"Delhi\",\n" + 
				"      \"createdDate\": \"2020-05-06 00:00:00\",\n" + 
				"      \"articles\": [\n" + 
				"        {\n" + 
				"          \"code\": \"Q"+sRndomValue+"\",\n" + 
				"          \"quantity\": 100000\n" + 
				"        }\n" + 
				"      ]\n" + 
				"    }\n" + 
				"  ]\n" + 
				"}";
	}
	
	public static String salesOrderList() {
		return "{\n" + 
				"	\"destination\": \"\",\n" + 
				"	\"origin\":\"DL1ANVR\",	\n" + 
				"	\"page\":1,\n" + 
				"	\"size\":50\n" + 
				"}";
	}
	
//	public static String stockCreate() {
//		return "{\n" + "\"code\":\"" + codeArticle + "\",\n" + "\"location\":\"DL1ANVR\",\n"
//				+ "\"quantity\":100000\n" + "}";
//	}
	
	public static String stockOrderList(){
		return "{\n" + 
				"\"code\":\"\",\n" + 
				"\"location\":\"\",\n" + 
				"\"page\":1,\n" + 
				"\"size\":25\n" + 
				"}";
	}
	
	public static String stockOrderUpdate() {
		return "{\n" + 
				"\"code\":\"Q640.7289960071300221\",\n" + 
				"\"location\":\"DL1ANVR\",\n" + 
				"\"quantity\":200000\n" + 
				"}";
	}

	public static String getRandomValue() {
		double randomValue = Math.random();
		String sRndomValue = randomValue + "";
		return sRndomValue;
		}
}
